/*
 * uv-timer.cpp
 *
 * Created: 06.03.2019 23:10:42
 * Author : domatskiy
 */ 

#define F_CPU 			1000000UL

//=========================================
// LCD
//=========================================

#define LCD_DATA_PORT	PORTC
#define LCD_DATA_DDR	DDRC

#define LCD_D4_PIN		PC5
#define LCD_D5_PIN		PC4
#define LCD_D6_PIN		PC3
#define LCD_D7_PIN		PC2

#define LCD_SYNC_PORT	PORTD
#define LCD_SYNC_DDR	DDRD

#define LCD_E_PIN		PD1
#define LCD_RS_PIN		PD2

//=========================================
// BUTTONS
//=========================================

#define BUTTON_PORT				PIND

#define BUTTON_START_PIN		PD5
#define BUTTON_TIME_UP_PIN		PD6
#define BUTTON_TIME_DOWN_PIN	PB7

#define KEY_START					0b00000001
#define KEY_TIME_UP					0b00000010
#define KEY_TIME_DOWN				0b00000100

#define DEBOUNCE_TIME 					20

//=========================================
// UF LED
//=========================================

#define UV_LED_PORT		PORTB
#define UV_LED_PIN		PB1

//=========================================
// BUZZER
//=========================================
#define BUZZER_PORT	PORTD
#define BUZZER_PIN	PD0


#include <stdio.h>
#include <avr/io.h>
#include <avr/eeprom.h>
#include <util/delay.h>
#include <avr/interrupt.h>
#include "lcd1602A.c"


//=========================================
// functions
//=========================================

void init_io();
void init_timer();
void flash();


void handle_buttons();
void handle_button(int key);

void dispaly();

void uv_on();
void uv_off();

void sound_on(unsigned int interval);
void sound_off();

volatile unsigned int Timer1 = 0;
volatile unsigned int Timer1_default = 120;
volatile unsigned int Timer1_last = 0;

volatile unsigned int _buzzer = 0; // время включения звонок
volatile unsigned int _pressed = 0; // нажата кнопка
volatile unsigned char _started = 0; // стартануло
volatile unsigned char _need_display_update = 0; // нужно обновить дисплей
volatile unsigned char _display_bizy = 0; // дисплей занят
volatile unsigned int percent_last = 0; // дисплей занят

volatile int time_delta = 5; // sec
unsigned int Timer1_eeprom EEMEM; // определяем переменную в EEPROM

// прерывания раз в секунду
// Прерывание по сравнению, канал A таймера/счетчика 1
ISR (TIMER1_COMPA_vect)
{
	if (Timer1 > 0 && _started > 0)
	{	
		Timer1--;
		
		if (Timer1 == 0) {
			uv_off();
			_started = 0;
			sound_on(5000);
		}

		_need_display_update = 1;
	}
}

// высокочастотные прерывания
// Прерывание по сравнению, канал A таймера/счетчика 2
ISR (TIMER2_COMPA_vect) 
{
	if (_buzzer > 1) 
	{
		_buzzer--;
	} 
	else if (_buzzer == 1) 
	{
		sound_off();
	}
}

void sound_on(unsigned int interval)
{
	BUZZER_PORT |= (1 << BUZZER_PIN); // 1 - на порт
	_buzzer = interval * 1;
}

void sound_off()
{
	BUZZER_PORT &= ~(1 << BUZZER_PIN); // 0 - на порт
	_buzzer = 0;
}

void uv_on()
{
	UV_LED_PORT |= (1 << UV_LED_PIN);
}

void uv_off()
{
	UV_LED_PORT &= ~(1 << UV_LED_PIN);
}

void dispaly()
{
	
	if(_need_display_update == 0 || _display_bizy == 1) {
		return;	
	}
	
	_display_bizy = 1;
	_need_display_update = 0;
	
	char timer_buffer[16];
	unsigned int min = Timer1 / 60;
	unsigned int sec = Timer1 - min * 60;
	
	char timer_buffer2[16];
	// lcd_com(0x01);
	
	if(_started > 0 && Timer1 > 0UL) 
	{
		// отображение прогресса 
		
		sprintf(timer_buffer, "LEFT TIME: %02u:%02u ", min, sec);
		
		unsigned int percent = 16 - (Timer1 * 16) / Timer1_last;

		for (unsigned char i = 0; i < 16; i++) {
			if(i < percent) {
				timer_buffer2[i] = 0xFF; // заполенный знак
				} else {
				timer_buffer2[i] = 0b00101101; // знак тире
			}
		}
		
		
		//sprintf(timer_buffer2, "Progress: %02u%s", percent, "%");
	} else if(_started == 0 && Timer1 == 0UL) { // завершон
		lcd_com(0x01);
		sprintf(timer_buffer, "LEFT TIME: %02u:%02u ", min, sec);
		sprintf(timer_buffer2, "%16s", "success         ");
	}
	else if(_started == 0 && Timer1 > 0UL)
	{
		lcd_com(0x01);
		sprintf(timer_buffer, "TIME: %02u:%02u ", min, sec);
		sprintf(timer_buffer2, "%16s", "start / set time");
	}
	else
	{
		lcd_com(0x01);
		sprintf(timer_buffer, "undefined app status %s", "");
		sprintf(timer_buffer2, "%s", "                ");
	}

	lcd_string(0x80, timer_buffer);
	lcd_string(0xC0, timer_buffer2);
	
	_display_bizy = 0; // освобождаем
}

void init_io()
{
	DDRB |= (1 << UV_LED_PIN); // set as output
	
	DDRD |= (1 << BUZZER_PIN); // set as output
	DDRD &= ~(1 << BUTTON_START_PIN | 1 << BUTTON_TIME_UP_PIN | 1 << BUTTON_TIME_DOWN_PIN); // D0, D1, D2 - set as input	
	PORTD |= (1 << BUTTON_START_PIN | 1 << BUTTON_TIME_UP_PIN | 1 << BUTTON_TIME_DOWN_PIN); //pull up resistor
}

void init_timer()
{
	// timer interrupts

	// Биты CS12 (2), CS11 (1), CS10 (0) регистра TCCR1B устанавливают режим тактирования и предделителя тактовой частоты таймера/счетчика T1:
	// 000 - таймер/счетчик T1 остановлен
	// 001 - тактовый генератор CLK
	// 010 - CLK/8
	// 011 - CLK/64
	// 100 - CLK/256
	// 101 - CLK/1024
	// 110 - внешний источник на выводе T1 (11 ножка) по спаду сигнала
	// 111 - внешний источник на выводе T1 (11 ножка) по возрастанию сигнала

	/************************************************************************/
	/* иничиализация 0го счетчика                                           */
	/************************************************************************/
	//TCCR0B |= (1<<WGM12); // CTC mode. (сброс по совпадению)
	//TCCR0B |= (1 << CS10); // 1 mgz
	//OCR0A = 100;
	//TIMSK0 |= (1<<OCIE1A);
	
	//TCCR0A |= (1<<WGM01); //CTC
	//TCCR0B |= (1<<CS02)|(1<<CS01)|(1<<CS00);// prescaler 1024
	//OCR0A = 10;
	//TIMSK0 |= (1<<OCIE0A); //compare match

	/************************************************************************/
	/* иничиализация 1го счетчика                                           */
	/************************************************************************/
	TCCR1B |= (1<<WGM12); // CTC mode. (сброс по совпадению)
	
	// Делитель частоты счетчика 8 (CS12:10 - 010).
	// При частоте 16 MHz - 0,5 мкС один такт.
	TCCR1B |= ((1 << CS12) | (1 << CS10)); // 1024
	//TCCR1B |= (1 << CS12); // делитель 256
	// TCCR1B |= ((1 << CS11) | (1 << CS10)); // делитель 64

	OCR1A  = 950;   // Верхняя граница счета. Диапазон от 0 до 65535.
					// Частота прерываний будет = Fclk/(N*(1+OCR1A))
					// где N - коэф. предделителя (1, 8, 64, 256 или 1024)
	
	TIMSK1 |= (1<<OCIE1A); //устанавливаем бит разрешени¤ прерывани¤ 1ого счетчика по совпадению с OCR1A(H и L)
	
	/************************************************************************/
	/* иничиализация 2го счетчика                                           */
	/************************************************************************/
	TCCR2B |= (1<<WGM12); // CTC mode. (сброс по совпадению)
	
	TCCR2B |= ((1 << CS10)); // CLK
	OCR2A = 1; 
	TIMSK2 |= (1<<OCIE2A); //устанавливаем бит разрешени¤ прерывания 2ого счетчика по совпадению с OCR2A(H и L)
}


void handle_buttons()
{
	handle_button(KEY_START);
	handle_button(KEY_TIME_UP);
	handle_button(KEY_TIME_DOWN);
}

void handle_button(int key)
{
	unsigned int pin = 0;
	
	// получаем пин
	switch (key) {
		case KEY_START:
			pin = BUTTON_START_PIN;
			break;
		
		case KEY_TIME_UP:
			pin = BUTTON_TIME_UP_PIN;
			break;
		
		case KEY_TIME_DOWN:
			pin = BUTTON_TIME_DOWN_PIN;
			break;
		
		default:
			break;
	}
	
	if (pin == 0) {
		return;
	}

	// 0 - pressed
	if ((BUTTON_PORT&(1<<pin)) == 0x00) 
	{

		// ранее не нажата ?
		if ((_pressed&key) == 0b00000000) 
		{	
			// ждем, анитидребезг контактов
			_delay_ms(DEBOUNCE_TIME);
		
			// 0 - pressed
			if ((BUTTON_PORT&(1 << pin)) == 0x00) 
			{
				_pressed |= key;

				if(!_started && Timer1 == 0) 
				{
					// если не запущен и таймер = 0
					// восстанавливаем значение
					Timer1 = Timer1_default;
				}

				// key action
				switch (key) 
				{
					case KEY_START: // старт / стоп
	
						if(_started == 0) 
						{
							sound_on(300);
							// включаем диоды
							uv_on();
							Timer1_last = Timer1; // сохраняем исходное время
							_started = 1; // переключаем
							// сохраням значение
							eeprom_write_word(&Timer1_eeprom, Timer1); // записываем "Timer1" в EEPROM
						} 
						else 
						{
							sound_on(100);
							Timer1 = (Timer1 / 10) * 10; // округляем
							uv_off();
							_started = 0;
						}

						_need_display_update = 1;
						break;
				
					case KEY_TIME_UP:
				
						if (_started) 
						{
							sound_on(1000);  // долгий сигнал
							return;
						}

						sound_on(300);
				
						if(Timer1 < 600) // 1.5x
							Timer1 += time_delta;
							
						_need_display_update = 1;
				
						break;
				
					case KEY_TIME_DOWN:
				
						if(_started) {
							sound_on(1000); // долгий сигнал
							return;
						}
				
						sound_on(300);
				
						if(Timer1 > time_delta) {
							Timer1 -= time_delta;
						} else {
							Timer1 = 0;
						}
						
						_need_display_update = 1;

						break;
				}
				
			
			}	
		}
	}
	else
	{
		// не нажата
		// выключаем бит
		_pressed &= ~key;
	}
}

int main(void)
{
	init_io();
	
	_delay_ms(100);
	
	// инициализация дисплея
	lcd_init();

	init_timer();
	sei();

	uv_off();	
	sound_on(200);
	
	lcd_com(0x01); // clear LCD
	//lcd_string(0xC0 ,"");
	
	lcd_string(0x80 ," = UV TIMERT =");

	_delay_ms(20);
	
	// считываем байт из EEPROM
	Timer1_default = eeprom_read_word(&Timer1_eeprom);
	
	if(Timer1_default > 600) 
	{
		Timer1_default = 60; // 2 мин
	} 
	else if (Timer1_default < 60) 
	{
		Timer1_default = 30; // 1 мин
	}

	Timer1 = Timer1_default;
	
	dispaly();
	uv_off();
	_delay_ms(100);
	_need_display_update = 1;
	
	while (1)
	{
		handle_buttons();
		_delay_ms(10);
		dispaly();
	}
}
